<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>c5</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Small</formFactors>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>College</label>
    <navType>Standard</navType>
    <tabs>Student__c</tabs>
    <tabs>Course__c</tabs>
    <tabs>Sports__c</tabs>
    <tabs>Elective_Subjects__c</tabs>
    <tabs>Other_facility__c</tabs>
    <tabs>KPI_TCI__c</tabs>
    <tabs>Incentive__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>College_UtilityBar1</utilityBar>
</CustomApplication>
