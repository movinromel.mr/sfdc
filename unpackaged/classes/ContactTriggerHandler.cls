/**
 * @description       : Apex Trigger for maintaining number of contacts on Account
 * @author            : Jonathan Hsu
 * @group             : 
 * @last modified on  : 06-26-2021
 * @last modified by  : Jonathan Hsu
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   06-26-2021   Jonathan Hsu   Initial Version
**/
public without sharing class ContactTriggerHandler {

    /**
    * @description Create a Set<Id> of accountIds from List<Contact> 
    * @author Jonathan Hsu | 06-26-2021 
    * @param contacts 
    * @return Set<Id> 
    **/
    private Set<Id> getAccountIds(List<Contact> contacts) {
        Set<Id> accountIds = new Set<Id>();
        
        for(Contact c : contacts) {
            if(c.AccountId != null) {
                accountIds.add(c.AccountId);
            }
        }
        
        return accountIds;
    }
    
    /**
    * @description SOQL query to retrieve number of contacts 
    * @author Jonathan Hsu | 06-26-2021 
    * @param accountIds 
    * @return Map<Id, Account> 
    **/
    private Map<Id, Account> getAccountMap(Set<Id> accountIds) {
        return new Map<Id, Account>([SELECT Id, Number_of_Contacts__c FROM Account WHERE Id in :accountIds]);
    }

    /**
    * @description Main execution: increments on INSERT/UNDELETE, decrements on DELETE
    * @author Jonathan Hsu | 06-26-2021 
    * @param contacts 
    * @param operation 
    **/
    public void process(List<Contact> contacts, System.TriggerOperation operation) {
        Set<Id> accountIds = getAccountIds(contacts);
        
        if(accountIds.size() > 0) {
            Map<Id, Account> accountMap = getAccountMap(accountIds);
            
            for(Contact c : contacts) {
                if(accountMap.containsKey(c.AccountId)) {
                    switch on operation{
                        when AFTER_INSERT {
                            accountMap.get(c.AccountId).Number_of_Contacts__c += 1;
                        }
                        when AFTER_DELETE {
                            accountMap.get(c.AccountId).Number_of_Contacts__c -= 1;
                        }
                        when AFTER_UNDELETE {
                            accountMap.get(c.AccountId).Number_of_Contacts__c += 1;
                        }
                    }
                }
            }
            
            update accountMap.values();
        }
    }
}