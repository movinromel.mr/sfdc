/**
 * @description       : Contact Trigger to update Number of Contacts
 * @author            : Jonathan Hsu
 * @group             : 
 * @last modified on  : 06-26-2021
 * @last modified by  : Jonathan Hsu
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   06-26-2021   Jonathan Hsu   Initial Version
**/
trigger ContactTrigger on Contact (before insert, after insert, after delete, after undelete) {
    System.debug(Trigger.operationType);
    
    ContactTriggerHandler handler = new ContactTriggerHandler();
    switch on Trigger.operationType {
        when AFTER_INSERT {
            handler.process(Trigger.new, Trigger.operationType);
        }
        when AFTER_DELETE {
            handler.process(Trigger.old, Trigger.operationType);
        }
        when AFTER_UNDELETE {
            handler.process(Trigger.new, Trigger.operationType);
        }
    }
}